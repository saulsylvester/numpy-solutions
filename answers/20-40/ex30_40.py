import numpy as np

## ex30. Compute the softmax score

url = '../iris.csv'
sepallength = np.genfromtxt(url, delimiter=',', dtype='float', usecols=[0])

def softmax(x):
	e_x = np.exp(x - np.max(x))
	return e_x / e_x.sum(axis=0)

print(softmax(sepallength))

## ex31. How to find the percentile scores of a numpy array?
np.percentile(sepallength, q=[5,95])

## ex32. How to insert values at random positions in an array?
iris_2d = np.genfromtxt(url, delimiter=',', dtype='object')

i, j = np.where(iris_2d)
np.random.seed(100)
iris_2d[np.random.choice((i),20),np.random.choice((i),20)] = np.nan

## ex33. How to find the position of missing values in numpy array?
iris_2d = np.genfromtxt(url, delimiter=',', dtype='float', usecols=[0,1,2,3])
iris_2d[np.random.randint(150, size=20), np.random.randint(4, size=20)] = np.nan

print("Number of missing values: \n", np.isnan(iris_2d[:, 0]).sum())
print("Position of missing values: \n", np.where(np.isnan(iris_2d[:, 0])))

## ex34. How to filter a numpy array based on two or more conditions?

condition = (iris_2d[:,2] > 1.5) & (iris_2d[:,0] < 5)
iris_2d[condition]

## ex35. How to drop rows that contain a missing value from a numpy array?

iris_2d[np.sum(np.isnan(iris_2d), axis = 1) == 0][:5]

##ex36. How to find the correlation between two columns of a numpy array?
iris = np.genfromtxt(url, delimiter=',', dtype='float', usecols=[0,1,2,3])
np.corrcoef(iris[:, 0], iris[:, 2])[0, 1]

##ex37. How to find if a given array has any null values?
iris_2d = np.genfromtxt(url, delimiter=',', dtype='float', usecols=[0,1,2,3])
np.isnan(iris_2d).any()

##ex38. How to replace all missing values with 0 in a numpy array?
iris_2d = np.genfromtxt(url, delimiter=',', dtype='float', usecols=[0,1,2,3])
iris_2d[np.random.randint(150, size=20), np.random.randint(4, size=20)] = np.nan
iris_2d[np.isnan(iris_2d)] = 0


##ex39. How to find the count of unique values in a numpy array?
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')

# Extract the species column as an array
species = np.array([row.tolist()[4] for row in iris])

# Get the unique values and the counts
np.unique(species, return_counts=True)

##ex40. How to convert a numeric to a categorical (text) array?
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')

# Bin petallength 
petal_length_bin = np.digitize(iris[:, 2].astype('float'), [0, 3, 5, 10])

# Map it to respective category
label_map = {1: 'small', 2: 'medium', 3: 'large', 4: np.nan}
petal_length_cat = [label_map[x] for x in petal_length_bin]
