# coding: utf-8
a = np.array([5, 7, 9, 8, 6, 4, 5])
b = np.array([6, 3, 4, 8, 9, 7, 1])
def maxx(x,y):
  if x > y:
    return x
  elif y > x:
    return y
  else:
    return "They are the same"
  
np.array([maxx(r,t) for r,t in zip(a,b)])
get_ipython().run_line_magic('save', 'ex15 1-10')
