import numpy as np
## How to stack two arrays vertically?
a = np.arange(10).reshape(2,-1)
b = np.repeat(1,10).reshape(2,-1)
np.concatenate([a,b],axis=0)
